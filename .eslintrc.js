module.exports = {
  plugins: ['my-eslint-plugin'],
  rules: {
    'my-eslint-plugin/no-hardcoded-strings': 'warn'
  }
}
